# htmlXmlAnalyzer

Html parser, which allows you to find modified item from two given html files. 
Program find most suitable item by comparing attributes of elements and finding the intersections of them. 
The element with the most intersection attributes is a result. By default target element id=*make-everything-ok-button*
## Usage
```bash
cd htmlXmlAnalyzer
java -jar htmlXmlAnalyzer.jar [path-to-original-html] [path-to-modified-html] [target-element-id]
```

## Output
Output is a path to a modified element:
```bash
html>body[1]>div[0]>div[1]>div[2]>div[0]>div>div[1]>a[1]
```

## Used libriaries
[Jsop](https://jsoup.org/)
