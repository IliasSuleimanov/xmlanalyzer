import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesProxy {
  private static final String PROPERTIES_FILENAME = "analyzer";
  private static final String POSTFIX = ".properties";
  private static final Properties properties = new Properties();
  
  private static PropertiesProxy instance;
  
  public static PropertiesProxy getInstance() {
    if(instance == null) {
      instance = new PropertiesProxy();
    }
    return instance;
  }
  
  private PropertiesProxy() {
    InputStream in = this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILENAME + POSTFIX);
    try {
      properties.load(in);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  public String getProperty(String key) {
    return properties.getProperty(key);
  }
}
