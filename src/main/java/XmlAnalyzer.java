import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.Optional;

public class XmlAnalyzer {
  private static final Logger logger = Logger.getLogger(XmlAnalyzer.class);
  private static final PropertiesProxy properties = PropertiesProxy.getInstance();
  
  private static final String NOT_ENOUGH_PARAMETERS = "analyzer.messages.error.not-enough-parameters";
  private static final String TARGET_DEFAULT_ID = "analyzer.modified-element.default.id";
  private static final String INITIALIZING = "analyzer.messages.initializing";
  private static final String ENCODING = "analyzer.encoding";
  private static final String EXITING = "analyzer.messages.error.exiting";
  private static final String TARGET_ELEMENT = "analyzer.messages.target-element";
  private static final String INPUT_FILES  = "analyzer.messages.input-files";
  private static final String FOUND_ELEMENT  = "analyzer.messages.found-edited-element";
  private static final String ELEMENT_NOT_FOUND = "analyzer.messages.error.not-found";

  static {
    BasicConfigurator.configure();
  }
  
  public static void main(String[] args) {

    if(args.length<2) {
      logger.error(PropertiesProxy.getInstance().getProperty(
          NOT_ENOUGH_PARAMETERS));
      return;
    }
    
    String sourceFileName = args[0];
    String diffFileName = args[1];
    String elementId = properties.getProperty(TARGET_DEFAULT_ID);
    if(args.length == 3) {
      elementId = args[2];
    }
    
    logger.info(properties.getProperty(INITIALIZING));
    
    XmlParserService parserService;
    try {
      parserService = new XmlParserServiceImpl(
          sourceFileName, diffFileName, properties.getProperty(ENCODING)
      );
    } catch (IOException ex) {
      logger.error(properties.getProperty(EXITING));
      return;
    }
    
    logger.info(String.format(properties.getProperty(TARGET_ELEMENT), elementId));
    logger.info(String.format(properties.getProperty(INPUT_FILES), sourceFileName, diffFileName));
    
    Optional<Element> modifiedElement = parserService.findModifiedElement(
        elementId);
    
    if(modifiedElement.isPresent()) {
      String path = XmlParserServiceImpl.getAbsolutePathToElement(
          modifiedElement.get());
      logger.info(String.format(properties.getProperty(
          FOUND_ELEMENT), diffFileName, path));
    } else {
      logger.info(properties.getProperty(ELEMENT_NOT_FOUND));
    }
  }
  
}
