import org.jsoup.nodes.Element;

import java.util.Optional;

public interface XmlParserService {
  Optional<Element> findModifiedElement(String id);
}