import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class XmlParserServiceImpl implements XmlParserService {
  private static final String READING_ERROR = "analyzer.messages.error.reading";
  private static final String ELEMENT_ATTR_INFO = "analyzer.messages.element-attributes-info";
  private static final String LOOKING_FOR_SIMILAR = "analyzer.messages.looking-for-most-similar";
  private static final String MAP_SIZE = "analyzer.messages.size-of-intersection-map";
  private static final String EXTRACTING_MOST_SIMILAR = "analyzer.messages.extracting-most-similar";
  private static final String FOUND_MOST_SIMILAR = "analyzer.messages.found-similar-element";
  
  private static final PropertiesProxy properties = PropertiesProxy.getInstance();
  private static final Logger logger = Logger.getLogger(XmlParserServiceImpl.class);
  
  private Document sourceDocument;
  private Document diffDocument;

  public XmlParserServiceImpl(final String sourceFileName, final String diffFileName, final String encoding)
      throws IOException {
    try {
      sourceDocument = Jsoup.parse(new File(sourceFileName), encoding);
    } catch (IOException exception) {
      logger.error(String.format(properties.getProperty(
          READING_ERROR), sourceFileName));
      throw exception;
    }
    try {
      diffDocument = Jsoup.parse(new File(diffFileName), encoding);
    } catch (IOException exception) {
      logger.error(String.format(properties.getProperty(
          READING_ERROR), diffFileName));
      throw exception;
    }
  }

  public XmlParserServiceImpl(final Document sourceDocument, final Document diffDocument) {
    this.sourceDocument = sourceDocument;
    this.diffDocument = diffDocument;
  }

  public Optional<Element> findModifiedElement(String id) {
    Element elementById = sourceDocument.getElementById(id);
    if(elementById == null) {
      return Optional.empty();
    }
    Attributes sourceElementAttributes = elementById.attributes();
    
    logger.info(String.format(
        properties.getProperty(ELEMENT_ATTR_INFO), sourceElementAttributes));
    logger.info(properties.getProperty(LOOKING_FOR_SIMILAR));
    
    Map<Element, Attributes> elementsWithIntersectedAttributes = findMostSimilarElementsByAttributes(sourceElementAttributes);
    
    logger.info(String.format(
        properties.getProperty(MAP_SIZE), elementsWithIntersectedAttributes.size()));
    logger.info(properties.getProperty(EXTRACTING_MOST_SIMILAR));
    
    return extractMostSimilarElement(elementsWithIntersectedAttributes);
  }

  private Map<Element, Attributes> findMostSimilarElementsByAttributes(Attributes attributes) {
    Map<Element, Attributes> elementsWithIntersectedAttributes = new HashMap<>();
    for(Element element : diffDocument.getAllElements()) {
      Attributes intersectedAttributes = getAttributesIntersectionForElement(element, attributes);
      if(intersectedAttributes.size() > 0) {
        logger.info(String.format(
            properties.getProperty(FOUND_MOST_SIMILAR), intersectedAttributes));
        elementsWithIntersectedAttributes.put(element, intersectedAttributes);
      }
    }
    return elementsWithIntersectedAttributes;
  }

  private Attributes getAttributesIntersectionForElement(Element element, Attributes attributes) {
    Attributes result  = new Attributes();
    for(Attribute attribute : attributes) {
      if(element.attributes().hasKey(attribute.getKey()) &&
          element.attributes().get(attribute.getKey()).equals(attribute.getValue())) {
        result.put(attribute);
      }
    }
    return result;
  }
  
  private Optional<Element> extractMostSimilarElement(Map<Element, Attributes> attrIntersectionCount) {
    if(attrIntersectionCount.size() > 0) {
      Map.Entry<Element, Attributes> elementWithMaxIntersections = attrIntersectionCount
          .entrySet()
          .stream()
          .max(Comparator.comparing((e) -> {
            return e.getValue().size();
          })).get();
      return Optional.of(elementWithMaxIntersections.getKey());
    }
    return Optional.empty();
  }

  public static String getAbsolutePathToElement(Element element) {
    StringBuilder path = new StringBuilder();
    path.append(element.nodeName());
    if(element.siblingElements().size() > 0) {
      path.append('[').append(element.elementSiblingIndex()).append(']');
    }
    for(Element parentEl : element.parents()) {
      StringBuilder sb = new StringBuilder();
      if(parentEl.siblingElements().size() > 0) {
        sb.append('[').append(parentEl.elementSiblingIndex()).append(']');
      }
      path.insert(0, parentEl.nodeName() + sb + '>');
    }
    return path.toString();
  }
}
